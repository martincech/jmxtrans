FROM openjdk:8-alpine

RUN apk update \
   && apk add curl \
   && apk add gnupg \
   && apk add tini \
   && apk add bash \
   && apk add libgcc \
   && apk add libc6-compat

## grab gosu for easy step-down from root
ENV GOSU_VERSION 1.7

RUN curl -L -o /usr/local/bin/gosu "https://github.com/tianon/gosu/releases/download/$GOSU_VERSION/gosu-amd64" \
    && curl -L -o /usr/local/bin/gosu.asc "https://github.com/tianon/gosu/releases/download/$GOSU_VERSION/gosu-amd64.asc" \
    && export GNUPGHOME="$(mktemp -d)" \
    && chmod +x /usr/local/bin/gosu \
    && gosu nobody true

ENV JMXTRANS_HOME /usr/share/jmxtrans
ENV PATH $JMXTRANS_HOME/bin:$PATH
ENV JAR_FILE $JMXTRANS_HOME/lib/jmxtrans-all.jar
ENV HEAP_SIZE 512
ENV SECONDS_BETWEEN_RUNS 60
ENV CONTINUE_ON_ERROR false
ENV JSON_DIR /var/lib/jmxtrans

# Install jmxtrans
RUN addgroup jmxtrans \
   && adduser jmxtrans -s /bin/bash -h /usr/share/jmxtrans -S -D -G jmxtrans

WORKDIR ${JMXTRANS_HOME}
RUN mkdir -p ${JMXTRANS_HOME}/conf

COPY logback.xml ${JMXTRANS_HOME}/conf/logback.xml

RUN mkdir -p /usr/share/jmxtrans/lib/ \
    && JMXTRANS_VERSION=`curl http://central.maven.org/maven2/org/jmxtrans/jmxtrans/maven-metadata.xml | sed -n 's:.*<release>\(.*\)</release>.*:\1:p'` \
    && mkdir -p /var/log/jmxtrans \
    && wget -q http://central.maven.org/maven2/org/jmxtrans/jmxtrans/${JMXTRANS_VERSION}/jmxtrans-${JMXTRANS_VERSION}-all.jar \
    && mv jmxtrans-${JMXTRANS_VERSION}-all.jar ${JAR_FILE}

COPY docker-entrypoint.sh /
RUN chmod a+x /docker-entrypoint.sh

RUN apk add gettext python py-yaml

ENV KAFKA_HOSTS=kafka
ENV KAFKA_PORT=9581
ENV INFLUXDB_HOST=influxdb
ENV INFLUXDB_PORT=8086
ENV INFLUXDB_DATABASE=_kafka

RUN mkdir ${JSON_DIR}
COPY yaml2jmxtrans.py ${JMXTRANS_HOME}/tools/yaml2jmxtrans.py
COPY yaml2jmxtrans.sh ${JMXTRANS_HOME}/bin/yaml2jmxtrans
RUN chmod a+x ${JMXTRANS_HOME}/bin/* && chmod a+x ${JMXTRANS_HOME}/tools/* && chmod a+w ${JSON_DIR}

COPY *.in ./

ENTRYPOINT ["/docker-entrypoint.sh"]
CMD ["start-with-jmx"]


